package com.juvetic.movieatuh;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.ImageView;

/**
 * Created by Juvetic on 5/9/2016.
 */
public class ImageViewSquared extends ImageView {

    public ImageViewSquared(Context context) {
        super(context);
    }

    public ImageViewSquared(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(getMeasuredWidth(), getMeasuredWidth());
    }
}
